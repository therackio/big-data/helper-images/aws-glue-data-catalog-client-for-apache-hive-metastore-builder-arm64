# aws-glue-data-catalog-client-for-apache-hive-metastore-builder-arm64

Extends Java buildtools container with pached Hive versions in order to properly build AWS Glue Data Catalog Client for Apache Hive Metastore (https://github.com/awslabs/aws-glue-data-catalog-client-for-apache-hive-metastore)
