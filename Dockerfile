ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG HIVE1_VERSION
ENV HIVE1_VERSION=$HIVE1_VERSION

ARG HIVE2_VERSION
ENV HIVE2_VERSION=$HIVE2_VERSION

RUN \
  set -eux && \
  apt update && \
  apt install -y git patch && \
  cd / && \
  git clone https://github.com/apache/hive.git /opt/hive1 && \
  cd /opt/hive1 && \
  git fetch && \
  git checkout tags/rel/release-$HIVE1_VERSION && \
  wget https://issues.apache.org/jira/secure/attachment/12958417/HIVE-12679.branch-1.2.patch && \ 
  patch -p0 <HIVE-12679.branch-1.2.patch && \
  mvn clean install -DskipTests -Phadoop-2 && \
  cd / && \
  git clone https://github.com/apache/hive.git /opt/hive2 && \
  cd /opt/hive2 && \
  git fetch && \
  git checkout tags/rel/release-$HIVE2_VERSION && \
  wget https://issues.apache.org/jira/secure/attachment/12958418/HIVE-12679.branch-2.3.patch && \
  patch -p0 <HIVE-12679.branch-2.3.patch && \
  mvn clean install -DskipTests
